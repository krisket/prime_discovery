﻿using pd_lib;
using System;
using System.Diagnostics;
using System.Threading;

namespace pd_console
{
    class Program
    {
        // collection of primes
        //static List<int> primes = new List<int>();
        static int _highest;
        // time to run
        static int _timeLeft = 60;
        static int _curSecond;

        static void Main(string[] args)
        {            
            // initialize timer running @ 1 second intervals
            Timer t = new Timer(TimerCallback, null, 1000, 1000);
            var _sw = Stopwatch.StartNew();
            int i = 0;
            Console.CursorVisible = false;
            do
            {
                i++;
                if (_timeLeft >= 0)
                {
                    if (i.IsPrime())
                    {
                        //primes.Add(i);
                        _highest = i;                        
                        display();
                    }
                }
                else
                    t.Dispose();

            } while (_timeLeft > 0);
            _sw?.Stop();
            // wait for last write...
            Thread.Sleep(1000);

            Console.WriteLine($"\nThis process ran for {_sw.Elapsed}");

            Console.WriteLine("Press Enter to exit...");
            Console.CursorVisible = true;
            Console.ReadLine();
        }

        private static void display()
        {
            // one per second
            if (_curSecond == _timeLeft) return;
            //clearLine();
            Console.Write($"\rTime Left: {_timeLeft, -5} Highest Value: {_highest, -16}");
            _curSecond = _timeLeft;
        }

        // clears current console line
        private static void clearLine()
        {
            Console.CursorLeft = 0;
            Console.Write(new string(' ', Console.BufferWidth));
            Console.CursorLeft = 0;
        }

        private static void TimerCallback(object o)
        {
            if (_timeLeft <= 0) return;
            _timeLeft--;
            display();
        }
    }
}
