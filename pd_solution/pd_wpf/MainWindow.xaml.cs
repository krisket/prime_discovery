﻿using pd_lib;
using pd_wpf.Models;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace pd_wpf
{
    public partial class MainWindow : Window
    {
        static Timer t;
        DataModel dc = new DataModel() { SecondsRemain = 60 };
        Stopwatch _sw;

        public MainWindow()
        {
            DataContext = dc;
            InitializeComponent();
        }

        private void begin(object sender, RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = false;
            t = new Timer(TimerCallback, DataContext, 1000, 1000);
            _sw = Stopwatch.StartNew();
            beginFindPrimes();
        }
        private static void TimerCallback(object o)
        {
            var dm = o as DataModel;
            if (dm != null)
            {
                if (dm.SecondsRemain > 0)
                    dm.SecondsRemain--;
                else
                    t.Dispose();
            }
        }
        private void beginFindPrimes()
        {
            if (dc != null)
            {
                Task.Factory.StartNew(() =>
                {
                    do
                    {
                        ++dc.CurrentValue;
                        if (dc.CurrentValue.IsPrime())
                            dc.HighestValue = dc.CurrentValue;

                    } while (dc.SecondsRemain > 0);
                    _sw?.Stop();
                    Console.WriteLine($"This process ran for {_sw.Elapsed}");
                }).ContinueWith(t=> 
                {
                    Dispatcher?.Invoke(() => { run2(); });
                });
            }
        }

        private void run2()
        {
            var win2 = new MainWindow2();
            win2.Show();
        }
    }    
}
