﻿using pd_lib;
using pd_wpf.Models;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace pd_wpf
{
    public partial class MainWindow2 : Window
    {
        DataModel dc = new DataModel() { SecondsRemain = 60 };
        Stopwatch _sw;

        public MainWindow2()
        {
            DataContext = dc;
            InitializeComponent();
        }

        private void begin(object sender, RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = false;
            // much faster using dispatcherTimer
            var dt = new DispatcherTimer() { Interval = TimeSpan.FromSeconds(1) };
            dt.Tick += (s, arg) => 
            {
                if (dc != null)
                {
                    if (dc.SecondsRemain > 0)
                        --dc.SecondsRemain;
                    else
                    {
                        ((DispatcherTimer)s).Stop();
                    }
                }
            };
            dt.Start();
            _sw = Stopwatch.StartNew();
            beginFindPrimes();
        }
        
        private void beginFindPrimes()
        {
            if (dc != null)
            {
                Task.Factory.StartNew(() =>
                {
                    do
                    {
                        ++dc.CurrentValue;
                        if (dc.CurrentValue.IsPrime())
                            dc.HighestValue = dc.CurrentValue;

                    } while (dc.SecondsRemain > 0);
                    _sw?.Stop();
                    Console.WriteLine($"This process ran for {_sw.Elapsed}");
                });
            }
        }
    }
    
}
