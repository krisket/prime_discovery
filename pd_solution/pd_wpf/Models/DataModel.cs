﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace pd_wpf.Models
{
    public class DataModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        int secondsRemain;
        public int SecondsRemain { get { return secondsRemain; } set { if (value != secondsRemain) { secondsRemain = value; NotifyPropertyChanged(); } } }
        int highestValue;
        public int HighestValue { get { return highestValue; } set { if (value != highestValue) { highestValue = value; NotifyPropertyChanged(); } } }

        public int CurrentValue { get; set; } = 0;

        public DataModel()
        {
            secondsRemain = 60;
            highestValue = 0;
        }
    }
}
