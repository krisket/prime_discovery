﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pd_lib
{
    public static class Util
    {
        //public static bool IsPrime(int src)
        //{
        //    int boundary = (int)Math.Floor(Math.Sqrt(src));

        //    if (src == 1) return false;
        //    if (src == 2) return true;

        //    for (int i = 2; i <= boundary; ++i)
        //    {
        //        if (src % i == 0) return false;
        //    }

        //    return true;
        //}

        // add extension method
        public static bool IsPrime(this int src)
        {
            // get the max number to check against
            int maxCheck = (int)Math.Floor(Math.Sqrt(src));

            if (src == 1) return false;
            if (src == 2) return true;

            // check values for division
            for (int i = 2; i <= maxCheck; ++i)
            {
                if (src % i == 0)
                    return false;
            }

            return true;
        }
    }

}
