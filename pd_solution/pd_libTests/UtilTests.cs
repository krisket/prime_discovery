﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using pd_lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pd_lib.Tests
{
    [TestClass()]
    public class UtilTests
    {
        [TestMethod()]
        public void JunkTest()
        {
            // do nothing -> first test takes longer than anything
        }

        [TestMethod()]
        public void IsPrimeTestValidUsingExtension()
        {
            int val = 2;
            Assert.IsTrue(val.IsPrime());
        }
        [TestMethod()]
        public void IsPrimeTestValidUsingDirect()
        {
            int val = 2;
            Assert.IsTrue(Util.IsPrime(val));
        }
        [TestMethod()]
        public void IsNotPrimeTestValidUsingExtension()
        {
            int val = 4;
            Assert.IsFalse(val.IsPrime());
        }
        [TestMethod()]
        public void IsNotPrimeTestValidUsingDirect()
        {
            int val = 4;
            Assert.IsFalse(Util.IsPrime(val));
        }
    }
}